import requests
import sys
from pprint import pprint

API_URL = 'https://www.googleapis.com/youtube/v3/search'
QUERY = 'kitten'
YOUTUBE_URL = 'https://www.youtube.com/watch?v='

r = requests.get(API_URL, params={
        'part': 'snippet',
        'type': 'video',
        'eventType': 'live',
        'maxResults': 25,
        'q': QUERY,
        'order': 'rating',
        'key': sys.argv[1],
    }, headers={
        'Accept': 'application/json',
    })

if '--debug' in sys.argv:
    from pprint import pprint
    pprint(r.json())

for result in r.json()['items']:
    if result['snippet']['channelId'] == 'UCtBXsnjQv8hMd5xsQ1Nm3nw':
        continue
    print(f"{YOUTUBE_URL}{result['id']['videoId']}")
